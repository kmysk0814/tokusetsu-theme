//var mySwiper = new Swiper('.swiper-container', {
//    loop: true,
//    slidesPerView: 2,
//    spaceBetween: 10,
//    centeredSlides: true,
//    //    breakpoints: {
//    //        767: {
//    //            slidesPerView: 1,
//    //            spaceBetween: 0
//    //        }
//    //    },
//    navigation: {
//        nextEl: '.swiper-button-next',
//        prevEl: '.swiper-button-prev',
//    },
//    pagination: {
//        el: '.swiper-pagination',
//    },
//})

var itemSwipers = new Swiper('.swiper-item', {
    navigation: {
        speed: 400,
        spaceBetween: 100,
        loop: true,
    },
    pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: true,
    }
})

var swiper = new Swiper('.swiper-mobile', {
    navigation: {
        speed: 400,
        spaceBetween: 100,
        loop: true,
    },
    pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: true,
    },
    speed: 1000,
    autoplay: {
        delay: 3000
    },
});

var swiper_d = new Swiper('.swiper-container', {
    loop: true,
    slidesPerView: 2,
    spaceBetween: 10,
    paginationClickable: true,
    pagination: '.swiper-pagination',
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true
    },
    loopAdditionalSlides: 3,
    centeredSlides: false,
    allowTouchMove: true,
    speed: 1000,
    autoplay: {
        delay: 3000
    },
});

// slideToメソッドを実行する関数を定義
function slideThumb(index) {
    itemSwipers.slideTo(index);
}


// ナビ　カテゴリーのモーダルの表示
// var btn = document.getElementById('menu-item-468');
// var modal = document.getElementById('category_modal');


var modal = $('#category_modal');

$("#menu-item-468").click(function() {
        modal.fadeToggle(200).toggleClass('on');

});

// $("#menu-item-468").mouseleave(function() {
//         modal.delay(100).fadeOut(100);
// });

// $("#category_modal").mouseenter(function() {
//         modal.fadeIn(300);
// });
// $("#category_modal").mouseleave(function() {
//         modal.fadeOut(300);
// });

// $("body.backdrop").click(function() {
//         modal.fadeOut(300);
// });

$(function() {
    if (modal.attr('class') == 'on') {
$("div").not("#category_modal").click(function() {
        modal.fadeOut(300);
        console.log(hey);
    });
}
});

// btn.addEventListener('mouseenter', function () {
//     modal.style.display = 'block';
// })

// modal.addEventListener('mouseenter', function () {
//     modal.style.display = 'block';
// })

// modal.addEventListener('mouseleave', function () {
//     modal.style.display = 'none';
// })

// btn.addEventListener('mouseleave', function () {
//     modal.style.display = 'none';
// })


$("div.itemlist").mouseenter(function () {
    $(this).addClass("item_cap_show");
});

$("div.itemlist").mouseleave(function () {
    $(this).removeClass("item_cap_show");
});

// To top btn

$(function(){
    $('a[href*="#"], area[href*="#"]').not(".noScroll").click(function() {
 
        var speed = 400, // ミリ秒(この値を変えるとスピードが変わる)
            href = $(this).prop("href"), //リンク先を絶対パスとして取得
            hrefPageUrl = href.split("#")[0], //リンク先を絶対パスについて、#より前のURLを取得
            currentUrl = location.href, //現在のページの絶対パスを取得
            currentUrl = currentUrl.split("#")[0]; //現在のページの絶対パスについて、#より前のURLを取得
        //#より前の絶対パスが、リンク先と現在のページで同じだったらスムーススクロールを実行
        if(hrefPageUrl == currentUrl){
 
            //リンク先の#からあとの値を取得
            href = href.split("#");
            href = href.pop();
            href = "#" + href;
 
            //スムースクロールの実装
            var target = $(href == "#" || href == "" ? 'html' : href),
                position = target.offset().top, //targetの位置を取得
                body = 'body',
                userAgent = window.navigator.userAgent.toLowerCase();

var ua = navigator.userAgent;
var scrollTag;
if( ua.indexOf('OPR') !== -1 || ua.indexOf('Edge') !== -1 ) {
  body = 'body';
} else {
  body = ( !window.chrome && 'WebkitAppearance' in document.documentElement.style )? 'body' : 'html';
}
console.log(body);
　
            if (userAgent.indexOf('msie') > -1 || userAgent.indexOf('trident') > -1 || userAgent.indexOf("firefox") > -1 ) { /*IE8.9.10.11*/
                body = 'html';
            }
            $(body).animate({
                scrollTop: position
            }, speed, 'swing', function() {
                //スムーススクロールを行ったあとに、アドレスを変更(アドレスを変えたくない場合はここを削除)
                if(href != "#top" && href !="#") {
                    location.href = href;
                }
            });

            return false;
        }

    });
});


jQuery(function(){
   jQuery(window).scroll(function(){
      var obj_t_pos = jQuery('div.recommend_bg').offset().top;
      var scr_count = jQuery(document).scrollTop() + (window.innerHeight/2); // ディスプレイの半分の高さを追加
      if(scr_count > obj_t_pos){
         jQuery('header.banner').addClass('banner_scr');
      }else{
         jQuery('header.banner').removeClass('banner_scr');
      }
   })
});


$('.orderbox_close').on('click', function(){
    jQuery('.orderbox_wrapper').fadeOut('50', function() {
    });
});


