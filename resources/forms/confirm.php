

<div class="confirm_wrap container h-100 px-0 mb-md-5">

		<div class="confirm_cont d-flex p-4 justify-content-center h-100 rounded shadow-sm">
			<div class="user_card" style="width: 480px">
				<div class="d-flex justify-content-center form_container">
					<form action="<?php echo esc_url( home_url( '/' ) ); ?>complete-send/"  method="post">
				    	<input type="hidden" name="itemid" value="<?php echo $_POST['itemid']; ?>">
				    	<input type="hidden" name="name_id" value="<?php echo $_POST['name_id']; ?>">
				    	<input type="hidden" name="sei_gotantou" value="<?php echo $_POST['sei_gotantou']; ?>">
				    	<input type="hidden" name="name_gotantou" value="<?php echo $_POST['name_gotantou']; ?>">
				    	<input type="hidden" name="sei_furi" value="<?php echo $_POST['sei_furi']; ?>">
				    	<input type="hidden" name="mei_furi" value="<?php echo $_POST['mei_furi']; ?>">
				    	<input type="hidden" name="busyo" value="<?php echo $_POST['busyo']; ?>">
						<input type="hidden" name="mail" value="<?php echo $_POST['mail']; ?>">
						<input type="hidden" name="url" value="<?php echo $_POST['url']; ?>">
						<input type="hidden" name="number" value="<?php echo $_POST['number']; ?>">
						<input type="hidden" name="comment" value="<?php echo $_POST['comment']; ?>">
						<input type="hidden" name="location" value="<?php echo $_POST['location']; ?>">



						    <h1 class="h4 card-title h2 font-weight-bold text-center mt-5 pb-2"><i class="fas fa-check"></i>お問い合わせ内容の確認</h1>


							  <div class="card-body">
							    <p class="card-text font-weight-normal mb-4"><span class="font-weight-bold">お名前：</span><?php echo htmlspecialchars($_POST["name_id"], ENT_QUOTES) ?></p>
						   	    <p class="card-text font-weight-normal mb-4"><span class="font-weight-bold">担当者様 お名前：</span><?php echo htmlspecialchars($_POST["sei_gotantou"], ENT_QUOTES) ?> <?php echo htmlspecialchars($_POST["name_gotantou"], ENT_QUOTES) ?></p>
						   	    <p class="card-text font-weight-normal mb-4"><span class="font-weight-bold">担当者様 フリガナ：</span><?php echo htmlspecialchars($_POST["sei_furi"], ENT_QUOTES) ?> <?php echo htmlspecialchars($_POST["mei_furi"], ENT_QUOTES) ?></p>
						   	    <p class="card-text font-weight-normal mb-4"><span class="font-weight-bold">部署名：</span><?php echo htmlspecialchars($_POST["busyo"], ENT_QUOTES) ?></p>
						   	    <p class="card-text font-weight-normal mb-4"><span class="font-weight-bold">メールアドレス：</span><?php echo htmlspecialchars($_POST["mail"], ENT_QUOTES) ?></p>
						   	    <p class="card-text font-weight-normal mb-4"><span class="font-weight-bold">WebサイトURL：</span><?php echo htmlspecialchars($_POST["url"], ENT_QUOTES) ?></p>
						   	    <p class="card-text font-weight-normal mb-4"><span class="font-weight-bold">ご注文予定個数：</span><?php echo htmlspecialchars($_POST["number"], ENT_QUOTES) ?></p>
							    <p class="card-text font-weight-normal mb-2"><span class="font-weight-bold">お問い合わせ内容詳細</span></p>
							    <p class="p-4 mb-4" class="rounded shadow-sm"><?php echo nl2br(htmlspecialchars($_POST["comment"], ENT_QUOTES)) ?></p>
							    <button type="submit" class="sendbtn btn-lg mr-2 text-light"><i class="far fa-paper-plane"></i> 送信する</button>
							    <button type="button" class="btn btn-primaly text-muted" onClick="history.back();">戻る</button>
							  </div>

						</form>
				</div>
			</div>
		</div>
	</div>
