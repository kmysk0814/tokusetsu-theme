<div id="contact" class="container h-100 px-0 pb-5 mb-md-5">
    <div class="container-fluid">
        <!--
        <div class="text-center">
            <a href="https://originalgoods.jp/" target="_b">
                <img src="./img/logo.png" alt="orginalgoods.jp">
            </a>
        </div>
-->
    </div>
    <div class="d-flex px-4 justify-content-center h-100">
        <div class="user_card">
            <div class="d-flex justify-content-center form_container">
                <form action="<?php echo esc_url( home_url( '/' ) ); ?>confirm/" method="post" class="needs-validation" novalidate>
                    <!--                    <h1 class="h4 font-weight-bold py-4 mt-4"><i class="fas fa-question-circle"></i> 「<?php //echo htmlspecialchars($_POST["itemid"], ENT_QUOTES) ?>」についてのお問い合わせ</h1>-->
                    <div class="mb-2 p-4">
                        <h2 class="text-center m-5">CONTACT</h2>
                        <p class="mb-1"><small>・こちらのお問い合わせフォームは100個以上の大口ご注文用となります。</small><br></p>
                            <p class="mb-1" style="padding-left: 1em;text-indent: -1em;"><small>・お問い合わせ内容に準じ、弊社担当より折返しご連絡を差し上げます。</small></p>
                    </div>
                    <div class="form-group mb-5">
                        <label for="name">貴社名（個人名）<span class="align-text-bottom badge badge-danger">必須</span></label>
                        <input size="30" class="form-control" type="text" id="name" name="name_id">
                        <div class="invalid-feedback">
                            お名前をご入力ください
                        </div>
                        <small id="passwordHelpBlock" class="form-text text-muted">個人の方はショップ名、もしくは個人名をご記入ください。</small>
                    </div>
                    <p>ご担当者氏名　<span class="badge badge-danger align-text-bottom">必須</span></p>
                    <!-- <p><small id="passwordHelpBlock" class="form-text text-muted">ご担当者氏名をご記入ください。</small></p> -->
                    <div class="form-row mb-4">
                        <div class="col">
                            <label for="sei_gotantou">姓</label>
                            <input size="10" type="text" class="form-control" id="sei_gotantou" name="sei_gotantou" placeholder="織寺" >
                            <div class="invalid-feedback">
                                名字をご入力ください
                            </div>
                        </div>
                        <div class="col">
                            <label for="name_gotantou">名</label>
                            <input size="10" type="text" class="form-control" id="name_gotantou" name="name_gotantou" placeholder="奈留" >
                            <div class="invalid-feedback">
                                名前をご入力ください
                            </div>
                        </div>
                    </div>
                    <!-- <p><small id="passwordHelpBlock" class="form-text text-muted">ご担当者のフリガナをご記入ください。</small></p> -->
                    <div class="form-row mb-4">
                        <div class="col">
                            <label for="sei_furi">セイ</label>
                            <input size="10" type="text" class="form-control" id="sei_furi" name="sei_furi"  placeholder="オリデラ"  >
                            <div class="invalid-feedback">
                                名字のフリガナをご入力ください
                            </div>
                        </div>
                        <div class="col">
                            <label for="mei_furi">メイ</label>
                            <input size="10" type="text" class="form-control" id="mei_furi" name="mei_furi"  placeholder="ナル"   >
                            <div class="invalid-feedback">
                                名前のフリガナをご入力ください
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-5">
                        <label for="busyo">部署名</label>
                        <input size="25" type="text" class="form-control" id="busyo" name="busyo" placeholder="">
                                </div>
                        <div class="form-group">
                            <label for="mail">メールアドレス（半角）　<span class="badge badge-danger align-text-bottom">必須</span></label>
                            <input type="email" class="form-control" id="mail" name="mail" placeholder="origi@nal.goods" >
                            <div class="invalid-feedback">
                                メールアドレスをご入力ください
                            </div>
                        </div>
                        <div class="form-group mb-5">
                            <label for="url">WEBサイトURL</label>
                            <input type="url" class="form-control" id="url" name="url" placeholder="http://originalgoods.jp" >
                                </div>
                            <div class="form-group mb-5">
                                <label for="number">予定の個数をお聞かせください　<span class="badge badge-danger align-text-bottom">必須</span></label>
                                <input size="30" class="form-control" type="number" id="number" name="number" min="100" >
                                <div class="invalid-feedback">
                                    予定個数をご入力ください。(100個〜)
                                </div>
                                <small id="passwordHelpBlock" class="form-text text-muted">100個からのご注文となります。</small>
                            </div>
                            <div class="form-group mb-1">
                                <label for="comment">お問い合わせ内容詳細</label><br>
                                <textarea class="form-control" name="comment" rows="6" cols="75"></textarea>
                                <small id="passwordHelpBlock" class="form-text text-muted">ご希望納品日、使用用途、その他ご質問など、ございましたらご記入ください。</small>
                            </div>
                            <input type="hidden" name="itemid" value="<?php echo $_POST['itemid']; ?>">
                            <input type="hidden" name="location" value="<?php echo $_POST['location']; ?>">
                            <button type="submit" class="btn btn-dark"><i class="fas fa-check"></i> 確認する</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    (function() {
        window.addEventListener('load', function() {
            var forms = document.getElementsByClassName('needs-validation');
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();

</script>
