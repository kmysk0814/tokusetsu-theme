    <?php
      mb_language("Japanese");
      mb_internal_encoding("UTF-8");
      $to = "kume@mawcoltd.jp";
      $title = $_POST['itemid']."についてのお問い合わせがありました。";
      $from  = "From: info@originalgoods.jp\r\n";
      $content = "オリジナルグッズ.jp"."\n\n"."\n\n"
      .$_POST['itemid']."についてのお問い合わせがありました。"."\n"
      							 ."商品ページ：　".$_POST['location']."\n\n"
      							 ."\n\n"
      							 ."━━━━━━□■□ お問い合わせ内容 □■□━━━━━━"
      							 ."\n\n"
      							 ."社名（個人名）：　".$_POST['name_id']."\n"
      							 ."担当者氏名　：　".$_POST['sei_gotantou']." ".$_POST['name_gotantou']."\n"
      							 ."担当者氏名(フリガナ)　：　".$_POST['sei_furi']." ".$_POST['mei_furi']."\n"
      							 ."部署名：　".$_POST['busyo']."\n"."\n"
      							 ."メールアドレス：　".$_POST['mail']."\n"
      							 ."WEBサイトURL：　".$_POST['url']."\n"."\n"
      							 ."注文予定個数　".$_POST['number']."\n"."\n"
      							 ."お問い合わせ内容詳細"."\n"
      							 ."- - - - - - - - - - - - - - -"."\n"."\n"
      							 .$_POST['comment']."\n"."\n"
      							 ."━━━━━━━━━━━━━━━━━━━━━━━━━━━━"."\n"."\n"."\n"."\n"
      							 ."お問い合わせ日時　".date("Y/m/d H:i:s")."\n"."\n"
      							 ."※このメールはシステムからの自動送信です。";

      if(mb_send_mail($to, $title, $content, $from)){
        $mes = "メールを送信しました";
      } else {
        $mes = "メールの送信に失敗しました。時間をおいて再度送信いただくか、お電話にてお問い合わせください。";
      };
    ?>


    <?php
      mb_language("Japanese");
      mb_internal_encoding("UTF-8");
      $email = htmlspecialchars($_POST['mail'], ENT_QUOTES);
      $from  = "From: info@originalgoods.jp\r\n";
      $to = $email;
      $title = "お問い合わせ受け付けのお知らせ|オリジナルグッズ.jp 自動送信メール";
      $content = "※このメールはシステムからの自動送信メールです。"."\n\n"."\n\n"
      .$_POST['name_id']."様"
      							 ."\n\n"
      							 ."お世話になっております。"
      							 ."オリジナルグッズ.jpでございます。"."\n"
      							 ."以下の内容でお問い合わせを受け付けいたしました。"."\n"
      							 ."担当者が確認後、折り返しご連絡いたしますので、今しばらくお待ち下さい。"."\n"."\n"
      							 ."※営業時間外にいただいたお問い合わせに関してましては、"."\n"
      							 ."返答までお時間を頂戴する場合がございますが、何卒ご了承くださいませ。"."\n"
      							 ."\n\n"
      							 ."━━━━━━□■□ お問い合わせ内容 □■□━━━━━━"
      							 ."\n\n"
       							 ."社名または個人名：　".$_POST['name_id']."\n"
      							 ."担当者氏名　：　".$_POST['sei_gotantou']." ".$_POST['name_gotantou']."\n"
      							 ."担当者氏名(フリガナ)　：　".$_POST['sei_furi']." ".$_POST['mei_furi']."\n"
      							 ."部署名：　".$_POST['busyo']."\n"."\n"
      							 ."メールアドレス：　".$_POST['mail']."\n"
      							 ."WEBサイトURL：　".$_POST['url']."\n"."\n"
							     ."お問い合わせ商品：　".$_POST['itemid']."\n"
      							 ."商品ページ：　".$_POST['location']."\n\n"
      							 ."注文予定個数　".$_POST['number']."\n"."\n"
      							 ."お問い合わせ内容詳細"."\n"
      							 ."- - - - - - - - - - - - - - -"."\n"."\n"
      							 .$_POST['comment']."\n"."\n"
      							 ."━━━━━━━━━━━━━━━━━━━━━━━━━━━━"."\n"."\n"."\n"."\n"
      							 ."お問い合わせ日時　".date("Y/m/d H:i:s")."\n"
      							 ."※上記に誤りがある場合は、ご連絡ください。"."\n"."\n"."\n"

									."───────────────────────────────"."\n"
									."株式会社MAW/オリジナルグッズ"."\n"
									."OEM事業部"."\n"
									."\n"
									."〒542-0082"."\n"
									."大阪府大阪市中央区島之内1-22-20 堺筋ビルディング7F/8F"."\n"
									."TEL:06-6718-5225 　FAX: 06-6251-3308"."\n"
									."E-mail: info@originalgoods.jp"."\n"
									."───────────────────────────────"."\n";

      if(mb_send_mail($to, $title, $content, $from)){
        $mes = "メールを送信しました";
      } else {
        $mes = "メールの送信に失敗しました。時間をおいて再度送信いただくか、お電話にてお問い合わせください。";
      };
    ?>

<div class="complete_wrapper">
<div class="container h-100 px-0 pb-5 mt-md-5 mb-md-5 rounded shadow-sm">
<div class="container-fluid py-5">
<div class="text-center">
</div>
</div>

		<div class="d-flex p-4  justify-content-center h-100">
			<div class="user_card" style="width: 480px">
				<div class="">
						    <h1 class="h4 card-title h2 font-weight-bold text-center mt-5 pb-2"><i class="fas fa-check"></i> お問い合わせありがとうございます！</h1>
						    <div class="my-5">
    						<p class="font-weight-bold">以下の内容を送信しました。後日弊社担当よりご連絡を差し上げます。</p>
    						<p class="font-weight-bold">また以下の内容は、ご入力いただいたメールアドレスに送付いたしました自動送信メールでもご覧いただけます。</p>
    						</div>
							  <div class="card-body p-3 mb-5 rounded-lg shadow">
							  	<div class="row">
							  	<div class="col-md-5">
    							<p class="naiyou font-weight-bold my-4 py-2 text-left">お問い合わせ内容</p>
    							</div>
    							</div>
							    <p class="card-text font-weight-normal mb-4">お名前：<?php echo htmlspecialchars($_POST["name_id"], ENT_QUOTES) ?></p>
						   	    <p class="card-text font-weight-normal mb-4">担当者様 お名前：<?php echo htmlspecialchars($_POST["sei_gotantou"], ENT_QUOTES) ?> <?php echo htmlspecialchars($_POST["name_gotantou"], ENT_QUOTES) ?></p>
						   	    <p class="card-text font-weight-normal mb-4">担当者様 フリガナ：<?php echo htmlspecialchars($_POST["sei_furi"], ENT_QUOTES) ?> <?php echo htmlspecialchars($_POST["mei_furi"], ENT_QUOTES) ?></p>
						   	    <p class="card-text font-weight-normal mb-4">部署名：<?php echo htmlspecialchars($_POST["busyo"], ENT_QUOTES) ?></p>
						   	    <p class="card-text font-weight-normal mb-4">メールアドレス：<?php echo htmlspecialchars($_POST["mail"], ENT_QUOTES) ?></p>
						   	    <p class="card-text font-weight-normal mb-4">WebサイトURL：<?php echo htmlspecialchars($_POST["url"], ENT_QUOTES) ?></p>
						   	    <p class="card-text font-weight-normal mb-4">ご注文予定個数：<?php echo htmlspecialchars($_POST["number"], ENT_QUOTES) ?></p>
							    <p class="card-text font-weight-normal mb-2">お問い合わせ内容詳細</p>
							    <p class="p-4 mb-4" style="border:1px solid #ccc" class="rounded shadow-sm"><?php echo htmlspecialchars($_POST["comment"], ENT_QUOTES) ?></p>
							    <!-- <button type="submit" class="btn text-light" style="background:#ff6670">送信する</button> -->
							  </div>
							    <button type="button" class="comp-close btn-lg text-light shadow-sm" onClick="window.open('','_self').close();"><i class="fas fa-times"></i> 閉じる</button>
				</div>
			</div>
		</div>
	</div>
  </div>