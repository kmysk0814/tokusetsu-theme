<?php if(!wp_is_mobile()):?>
<section class="side-menu-section">
    <h3 class="side-menu-head font-weight-normal mb-0">商品カテゴリ一覧</h3>
    <ul id=menu-category-list class="pl-0 mb-0 side-menu-bar">
        <?php
        $defaults = array(
            'menu'            => '3',
            'menu_class'      => 'menu',
            'container'       => 'div',
            'container_class' => '',
            'container_id'    => '',
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'echo'            => true,
            'depth'           => 0,
            'walker'          => '',
            'theme_location'  => '',
            'items_wrap'      => '<ul id="category_list_ul">%3$s</ul>',
        );
        //wp_nav_menu( $defaults );
        ?>
        <?php
        $categories = get_categories();
        //$categories = get_categories('parent=0'); 親カテゴリーのみ
        foreach($categories as $category) :
        $cat_id = $category->cat_ID;
        $post_id = 'category_'.$cat_id;
        $catimg = get_field('category_image',$post_id);
        $img = wp_get_attachment_image_src($catimg, 'thumbnail');
        ?>
        <div class="">
            <div>
                <p class="font-weight-light"><a href="<?php echo esc_url( home_url( '/' ) ); ?>
category/<?php echo $category->slug; ?>"><?php echo $category->cat_name; ?></a></p>
            </div>
        </div>
        <?php endforeach; ?>
    </ul>
    <h3 class="side-menu-head font-weight-normal mb-0">サブメニュー</h3>
    <ul id=menu-category-list class="pl-0 mb-0 side-menu-bar font-weight-light">
        <?php
// 引数に指定したメニューの位置にメニューが設定してある場合。引数にはregister_nav_menus()で登録したスラッグ名を指定
if ( has_nav_menu( 'sub' )){
    // メニューの設定を配列で指定
    $args = array(
        // 表示させるメニューを、register_nav_menus()で登録したスラッグ名で指定。初期値はなし
        'theme_location' => 'sub',
        // ul要素を囲むかどうか。使えるタグはdiv、nav。囲まない場合はfalseを指定。初期値はdiv
        'container'      => 'false',
        // メニューのリンク前に表示するテキスト。初期値はなし
        'link_before'    => '<span>',
        // メニューのリンク後に表示するテキスト。初期値はなし
        'link_after'     => '</span>',
        // メニュー項目を囲むタグ。囲むタグをなしにする場合でも、パラメータを指定し %3$s の記述が必須
        'items_wrap'     =>'%3$s'
    );
    // メニューを表示
    wp_nav_menu( $args );
};
?>
    </ul>
</section>
<?php endif;?>
