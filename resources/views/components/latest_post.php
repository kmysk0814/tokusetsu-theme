<section class="latest_post">
    <div class="col-xs-12">
        <div class="text-center">
            <h2 class="h3 mt-5 head_news">NEWS</h2>
        </div>
        <div class="news_content_wrapper mb-2 px-1">
            <div class="news_content">
                <?php
                        $args = array(
                            'post_type' => 'news', // カスタム投稿タイプ Products
                            'post_status' => 'publish', // 公開済の投稿を指定
                            'posts_per_page' => 3 // 投稿件数の指定
                        );
                        $the_query = new WP_Query($args); if($the_query->have_posts()):
                        ?>
                <?php while ($the_query->have_posts()): $the_query->the_post(); ?>
                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <p class="news_date">
                        <?php echo get_the_date(); ?>
                    </p>
                    <div class="news_title">
                        <?php if( empty(get_the_content()) ) : // 本文を空白にした場合はリンクなし ?>
                        <span><?php the_title(); ?></span>
                        <?php elseif( empty(get_the_title()) ) : // タイトルが空白の場合は本文をタイトルとして表示 ?>
                        <?php remove_filter('the_content', 'wpautop'); ?><?php the_content(); ?>
                        <?php else : // 通常の投稿 ?>
                        <span><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></span>
                        <?php endif; ?>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
                <?php else: ?>
                <!-- 投稿が無い場合の処理 -->
                <p>記事はまだありません。</p>
                <?php endif; ?>
            </div> <!-- news_content -->
            <div class="news_all_btn">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>
news/">
                            <button type="button" class="btn btn-dark mt-1">NEWS一覧</button>
                        </a>
            </div>
        </div> <!-- news_content_wrapper -->
    </div>
</section>
