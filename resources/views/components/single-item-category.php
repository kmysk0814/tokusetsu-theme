<div id="category_component">
    <div class="text-center">
        <h3 class="related_head head_news">カテゴリー</h3>
    </div>
<div class="d-flex">
<?php
$defaults = array(
'menu'            => '3',
'menu_class'      => 'menu',
'container'       => 'div',
'container_class' => '',
'container_id'    => '',
'fallback_cb'     => 'wp_page_menu',
'before'          => '',
'after'           => '',
'link_before'     => '',
'link_after'      => '',
'echo'            => true,
'depth'           => 0,
'walker'          => '',
'theme_location'  => '',
'items_wrap'      => '<ul id="category_modal_ul" class="d-flex justify-content-start flex-wrap flex-column p-0 list-unstyled">%3$s</ul>',
);
//wp_nav_menu( $defaults );
?>
        <?php
$categories = get_categories();
//$categories = get_categories('parent=0'); 親カテゴリーのみ
foreach($categories as $category) :
$cat_id = $category->cat_ID;
$post_id = 'category_'.$cat_id;
$catimg = get_field('category_image',$post_id);
$img = wp_get_attachment_image_src($catimg, 'thumbnail');
?>
        <div class="category_wrap mb-2 mx-1">
            <div class="mb-2">
                 <a href="<?php echo esc_url( home_url( '/' ) ); ?>
category/<?php echo $category->slug; ?>"><img class="cat_thumb" src="<?php echo $catimg; ?>" alt="<?php echo $category->cat_name; ?>" /></a>
            </div>
            <div>
                <p class="font-weight-light small"><a href="<?php echo esc_url( home_url( '/' ) ); ?>
category/<?php echo $category->slug; ?>"><?php echo $category->cat_name; ?></a></p>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>
