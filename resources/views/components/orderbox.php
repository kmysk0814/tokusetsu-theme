<?php if(!wp_is_mobile()):?>
<div class="orderbox_wrapper round shadow-sm p-md-3">
    <p class="h6 font-weight-bold mb-md-4 text-center">大口のご注文をご希望のお客様へ</p>
    <p class="small">100個以上のロット注文をご希望のお客様に、専用のお問い合わせを設けております。</p>
    <p class="small">弊社スタッフが承り、通常2営業日以内にて返信いたします。こちらからお気軽にご相談ください。</p>
    <div class="text-center">
        <a type="button" class="btn btn-orderbox" role="button" href="<?php echo esc_url( home_url( '/' ) ); ?>#contact">大口注文の相談をする</a>
    </div>
    <div class="orderbox_close"><i class="far fa-times-circle"></i></div>
</div>
<?php endif; ?>
