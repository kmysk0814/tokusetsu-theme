<?php
/**
 * Template Name: CategoryPage
 */
?>
@extends('layouts.app')
@section('content')
@include('partials.page-header')
<div class="slider_padding">
    <section class="category_head_image mb-md-5" style="background-image:url('<?php echo esc_url( home_url( '/' ) ); ?>
wp-content/uploads/2019/11/back_news.png')">
    </section>
    <div class="container">
      <section id="categories_ichiran" class="content-area">
        <main id="main" class="site-main" role="main">
        <h1 class="h3 text-center py-4 entry-title">カテゴリー</h1>
        <?php
        $args = array(
          'orderby' => 'id',
          'order' => 'desc'
        );
        $categories = get_categories($args);
        foreach($categories as $category){
          echo "<h3 class="."categories_name"."><a href=\"".get_category_link($category->term_id)."\">".$category->name."</a></h3>\n";
          echo "<p>".$category->category_count."個の商品";
        } ?>
        </main><!-- #main -->
    </section><!-- #primary -->
	</div>
</div>
@endsection

