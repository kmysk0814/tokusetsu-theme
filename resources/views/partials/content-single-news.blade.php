<!-- newsの個別ページの記述 -->
<div class="slider_padding">
    <div class="cont_wrap single-item">
        <div class="row">
            <div class="col-md-8">
                @include('partials.news_view')
                <div class="pnbutton d-flex border-top border-bottom py-4">
                    <?php if (get_previous_post()):?>
                    <?php previous_post_link('%link', '<i class="fas fa-arrow-circle-left"></i> 前の記事へ'); ?>
                    <?php endif; ?>
                    <?php if (get_next_post()):?>
                    <?php next_post_link('%link', '次の記事へ <i class="fas fa-arrow-circle-right"></i>'); ?>
                    <?php endif; ?>
                </div>
            </div> <!-- right col- -->
            <div class="col-md-3">
               <?php if(!wp_is_mobile()): ?>
                @include('components.side-menu')
               <?php endif;?>
                @include('components.latest_post')
            </div>
        </div>
    </div>
</div> <!-- right col- -->
<div class="mt-5">
    <div class="cont_wrap single-item">
        @include('components.single-item-category')
    </div>
</div>
