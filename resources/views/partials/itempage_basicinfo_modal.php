
<div class="modal fade and carousel slide" id="lightbox">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-body">
<ol class="carousel-indicators">
<? $data_num =0; ?>
<?php while(have_rows('item_images')): the_row(); ?>

<li data-target="#lightbox" data-slide-to="<? echo $data_num ?>"
    <?php if($data_num == 0): echo 'class="active"';
    endif;?>
></li>

<?php $data_num+=1;?>
<?php endwhile;?>
</ol>
<div class="carousel-inner">
<? $data_num =0; ?>
<?php while(have_rows('item_images')): the_row(); ?>
    <div class="item<?php if($data_num == 0): echo ' active'; endif;?>">
        <img src="<?php (the_sub_field('item_image')); ?>" alt="<?php (the_sub_field('alt')); ?>" class="">
    </div>

<?php $data_num+=1;?>
<?php endwhile; ?>

</div><!-- /.carousel-inner -->
<a class="left carousel-control" href="#lightbox" role="button" data-slide="prev">
<span class="glyphicon glyphicon-chevron-left"></span>
</a>
<a class="right carousel-control" href="#lightbox" role="button" data-slide="next">
<span class="glyphicon glyphicon-chevron-right"></span>
</a>
</div><!-- /.modal-body -->
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
