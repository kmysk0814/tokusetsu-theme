<!-- newsの個別ページの記述 -->
<div class="">
    <div class="row">
        <div class="col-md-12">
            <h2 class="item_para font-weight-bold border-bottom border-dark py-3 mb-4">
                <?php (the_title());?>
            </h2>
            <div class="artivle_date py-2 border-bottom mb-5">
                <p class="font-weight-light">
                    <?php the_date(); ?>
                </p>
            </div>
            <section id="item_article">
                <?php (the_content()); ?>
            </section>
            <div class="border-top py-3">
                <?php require(__DIR__.'/../components/social.php');?>
            </div>
        </div> <!-- right col- -->
    </div>
</div>
