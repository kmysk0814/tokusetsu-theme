
<?php
// カテゴリを全て取得
$category = get_the_category();
// カテゴリIDを取得
$category_id = $category[0]->cat_ID;
// カテゴリ名を取得
$category_name = $category[0]->cat_name;
// スラッグを取得
$category_slug = $category[0]->category_nicename;
// カテゴリへのリンクを取得
$category_link = get_category_link($category_id);

$rows = get_field('item_images' ); // すべてのrow（内容・行）をいったん取得する
$first_row = $rows[0]; // 1行目だけを$first_rowに格納しますよ～
$first_row_image = $first_row['item_image']; // get the sub field value
$first_row_alt = $first_row['alt']; // get the sub field value
$imagesrc = wp_get_attachment_image_src( $first_row_image, 'full' );
?>
<article @php post_class() @endphp>
    <div class="item-archive-flexitem">
        <div class="item-archive-header">
            <a class="item-archive-imagelink" href="<?php the_permalink(); ?>">
                <img src="<?php echo $first_row_image ; ?>" alt="<?php echo $first_row_alt; ?>" class="">
            </a>
        </div>
        <h2 class="font-weight-bold mb-1 entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h2>
        <!-- リンク出力 -->
        <p class="badge badge-secondary text-white item-archive-cat">
            <!-- リンク文字を出力 -->
            <span>
                <?php echo $category_name; ?>
            </span>
        </p>
        <div class="entry-summary">
            <!-- @php the_excerpt() @endphp -->
            <p class="item_archive_price">
                <?php (the_field('basic_info2_item_grp_price_item_number_min_lot')); ?>
                <?php (the_field('basic_info2_item_grp_price_item_number_item_unit')); ?>
                税込
                <?php (the_field('basic_info2_item_grp_price_item_price')); ?>
                円〜
            </p>
        </div>
    </div>
</article>
