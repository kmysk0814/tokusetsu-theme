<section id="items" class="twin-clm-wrapper">
    <div class="twin-clm-content cont_wrap_items">
        <div class="text-center">
            <h2 class="mb-md-5 head_items">ITEMS</h2>
        </div>
        <div class="row mt-mb-5">
            <div class="col-xs-12">
                <?php require_once(__DIR__ .'/top_items_content.php');?>
            </div>
        </div>
    </div>
</section>
