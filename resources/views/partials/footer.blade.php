<footer class="content-info footer">
    <div class="container py-5">
        <div class="footer_wrapper d-flex">
            <div class="footer-container">
                <!-- 投稿 フッター ＞ お支払い方法 で管理 -->
                <?php $page = get_post(160);
            echo $page->post_content;?>
                <!-- 投稿 フッター ＞ お支払い方法 で管理 -->
            </div>
            <div class="footer-container">
                <!-- 投稿 フッター ＞ 配送方法 で管理 -->
                <?php $page = get_post(162);
            echo $page->post_content;?>
                <!-- 投稿 フッター ＞ 配送方法 で管理 -->
            </div>
            <div class="footer-container">
                <!-- 投稿 フッター ＞ サイト紹介 で管理 -->
                <?php $page = get_post(184);
            echo $page->post_content;?>
                <!-- 投稿 フッター ＞ サイト紹介 で管理 -->
            </div>
        </div>
        @include('components.to_top_btn')
    </div>
</footer>
<!--load all styles -->
<script src="https://unpkg.com/swiper/js/swiper.min.js"></script>
<script async src="<?php echo esc_url( home_url( '/' ) ); ?>
wp-content/themes/tokusetsu-theme/resources/assets/scripts/add_scripts.js"></script>
