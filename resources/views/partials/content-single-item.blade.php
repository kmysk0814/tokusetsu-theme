<div class="slider_padding">
    <div class="item_padding cont_wrap single-item">
        <div class="row">
            <div class="col-md-12">
                @include('partials.itempage-basicinfo')
                <!--
                <h3 class="fluid font-weight-bold p-3 mt-md-4 mb-0 mb-md-3 bg-dark text-white">
                    <i class="fas fa-bookmark"></i>　商品の特徴
                </h3>-->
                <section id="item_article">
                    <?php (the_content()); ?>
                </section>
                @include('partials.itempage-tabarea')
                @include('partials.itempage-pricearea')
                @include('partials.itempage-cartarea')
                @include('partials.itempage-relatedarea')
                @include('components.single-item-category')
                @include('components.orderbox')
            </div> <!-- right col- -->
        </div>
    </div>
</div>
