<div class="row">
    <div class="col-12 col-md-7">
        <div class="swiper-flex-item">
            <!--画像スライダーとサムネイルをインクルード-->
            <?php require_once dirname(__FILE__).'/itempage_basicinfo_image.php'?>
        </div>
    </div>
    <div class="col-12 col-md-5">
        <div class="item_disc_wrap px-3 px-md-5">
            <!--情報エリアをインクルード-->
            <?php require_once dirname(__FILE__).'/itempage_basicinfo_info.php'?>
        </div>
    </div>
</div>
<!--</div> image-col -->
<!--------------------------------------->
<!--画像モーダルのコンポーネントをインクルード-->
<!--------------------------------------->
<?php require_once dirname(__FILE__).'/itempage_basicinfo_modal.php'?>
