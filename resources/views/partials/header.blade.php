<header class="banner">
    <div class="container">
        <nav class="nav-primary">
            @if (has_nav_menu('primary_navigation'))
            <section class="navigation d-flex align-items-center">
                <a class="navbar-brand" href="http://yotsubaakurirulab1106.local">
                    <img src="<?php echo esc_url( home_url( '/' ) ); ?>
wp-content/uploads/2019/11/ac-logo.png">
                </a>
                {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
                @include('partials/category_modal')
            </section>
            @endif
        </nav>
    </div>
</header>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="http://yotsubaakurirulab1106.local"><img src="<?php echo esc_url( home_url( '/' ) ); ?>
wp-content/uploads/2019/11/ac-logo.png">
    </a>
    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#Navber" aria-controls="Navber" aria-expanded="false" aria-label="ナビゲーションの切替">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="Navber">
        {!! wp_nav_menu(['menu' => 'mobile_nav', 'menu_class' => 'navbar-nav mr-auto']) !!}
        <!--

       <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">ホーム <span class="sr-only">(現位置)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">リンク</a>
            </li>
            <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    ドロップダウン
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">メニュー1</a>
                    <a class="dropdown-item" href="#">メニュー2</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">その他</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">無効</a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <input type="search" class="form-control mr-sm-2" placeholder="検索..." aria-label="検索...">
            <button type="submit" class="btn btn-outline-success my-2 my-sm-0">検索</button>
        </form>
-->
    </div><!-- /.navbar-collapse -->
</nav>
