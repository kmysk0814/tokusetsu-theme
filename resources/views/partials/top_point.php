<section id="point">
<div class="col-10 offset-1 col-md-12 offset-md-0">
<div class="text-center">
<h2 class="h3 pt-md-5">ヨツバのアクリル３つの特徴</h2>
</div>
<div class="mb-0 py-3 py-md-5">

    <div class="d-flex justify-content-around point-flex">
        <div class="point_content px-lg-5 mt-4">
            <div class="point_img text-center mb-3 mb-md-5 shadow round">test</div>
            <div class="point_disc text-center">
                <h5 class="mb-md-3">高いクオリティの印刷を手軽にお届け</h5>
                <p>仕上がりへの満足度を第一に考え、自社工場のプリンターはどれも高品質なものを揃えています。</p>
            </div>
        </div>
        <div class="point_content px-lg-5 mt-4">
            <div class="point_img text-center mb-3 mb-md-5 shadow round">test</div>
            <div class="point_disc text-center">
                <h5 class="mb-md-3">高いクオリティの印刷を手軽にお届け</h5>
                <p>仕上がりへの満足度を第一に考え、自社工場のプリンターはどれも高品質なものを揃えています。</p>
            </div>
        </div>
        <div class="point_content px-lg-5 mt-4">
            <div class="point_img text-center mb-3 mb-md-5 shadow round">test</div>
            <div class="point_disc text-center">
                <h5 class="mb-md-3">高いクオリティの印刷を手軽にお届け</h5>
                <p>仕上がりへの満足度を第一に考え、自社工場のプリンターはどれも高品質なものを揃えています。</p>
            </div>
        </div>
    </div>
</div>
</div>
</section>
