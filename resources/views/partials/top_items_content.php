<section id="item-list-wrapper" class="mb-5">
    <div class="item-list-grid-content">
        <?php
  query_posts('posts_per_page=3');
  $args = array( 'post_type' => 'item', 'posts_per_page' => 0,
);
  $the_query = new WP_Query($args);

  if($the_query->have_posts()):

    while ($the_query->have_posts()): $the_query->the_post();
    $rows = get_field('item_images' ); // すべてのrow（内容・行）をいったん取得する
    $first_row = $rows[0]; // 1行目だけを$first_rowに格納しますよ～
    $first_row_image = $first_row['item_image']; // get the sub field value
    $imagesrc = wp_get_attachment_image_src( $first_row_image, 'full' );
?>
        <!-- 各カード -->
        <div id="post-<?php the_ID(); ?>" <?php post_class('item-list-grid-item'); ?>>
            <div class="row mx-md-3 my-md-4">
                <div class="col-12 col-md-12 col-lg-12 px-0">
                    <div class="itemlist">
                        <!-- アイキャッチ表示 -->
                       <div class="img-container">
                            <a href="<?php the_permalink(); ?>" class="item-list-eyecatch">
                                <img src="<? echo $first_row_image ?>" alt="<? echo $first_row['alt'] ?>" class="item_image">
                            </a>
                        </div>
                        <div class="item_discription">
                            <!-- タイトル -->
                            <h5 class="text-center mt-md-3 font-weight-bold">
                                <a class="text-dark" href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a>
                            </h5>
                            <!-- 値段 -->
                            <div class="item-discription-low">

                                <!-- カテゴリボタン -->
                                <div>
                                    <?php
                                    // カテゴリを全て取得
                                    $category = get_the_category();
                                    // カテゴリIDを取得
                                    $category_id = $category[0]->cat_ID;
                                    // カテゴリ名を取得
                                    $category_name = $category[0]->cat_name;
                                    // スラッグを取得
                                    $category_slug = $category[0]->category_nicename;
                                    // カテゴリへのリンクを取得
                                    $category_link = get_category_link($category_id);
                                    ?>
                                    <div class="post-meta">
                                        <p class="post-category">
                                            <!-- リンク出力 -->
                                            <a href="<?php echo $category_link; ?>">
                                                <!-- リンク文字を出力 -->
                                                <?php echo $category_name; ?>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                                <div class="item-list-price">
                                    <?php $lot = get_post_meta( get_the_ID(), 'basic_info2_item_grp_price_item_number_min_lot', true );
                                    $unit = get_post_meta( get_the_ID(), 'basic_info2_item_grp_price_item_number_item_unit', true );
                                    $price = get_post_meta( get_the_ID(), 'basic_info2_item_grp_price_item_price', true );

                                    echo $lot . $unit ." " .$price ."円"
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
        <?php else: ?>
        <p>まだ投稿がありません。</p>
        <?php endif; ?>
    </div>
</section>
