<section id="news_wrapper" class="mb-0">
    <div id="newsguide" class="newslist cont_wrap">
        <div class="row">
            <div class="col-12 col-md-12 offset-md-0">
                <div class="text-center">
                    <h2 class="mt-mb-5 head_items">NEWS</h2>
                </div>
                <div class="news_content_wrapper mb-5 py-3">
                    <div class="news_content">
                        <? require_once dirname(__FILE__) . '/top_news_content.php' ?>
                    </div>
                    <div class="news_all_btn">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>
news/">
                            <button type="button" class="btn btn-dark">NEWS一覧</button>
                        </a>
                    </div>
                </div> <!-- news_content_wrapper -->
            </div>


        </div>
    </div>
</section>
