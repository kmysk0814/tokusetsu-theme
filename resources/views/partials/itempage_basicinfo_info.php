<h1 class="item_title font-weight-bold pb-2">
    <?php (the_field('basic__item_name')); ?>
</h1>
<p class="item_cap">
    <?php (the_field('basic__item_cap')); ?>
</p>
<h3 class="item_price_p">
    <span class="item_unit_bold font-weight-bold">
<?php (the_field('basic_info2_item_grp_price_item_number_min_lot')); ?>
<?php (the_field('basic_info2_item_grp_price_item_number_item_unit')); ?>
</span>
    税込
    <span class="item_price_bold font-weight-bold">
<?php (the_field('basic_info2_item_grp_price_item_price')); ?>
</span>円〜
</h3>
<p class="font-weight-light text-muted">
    <?php (the_field('basic__item_spec')); ?>
</p>
<div class="row">
    <div class="d-flex flex-row">
        <div class="badge badge-light text-wrap p-3 mr-1">
            送料　<?php (the_field('basic_info2__item_deli'));?>円〜
        </div>
        <div class="badge badge-light text-wrap p-3">
            最短納期　<?php (the_field('basic_info2__item_nouki')); ?>営業日より
        </div>
    </div>
</div>
<a href="<?php (the_field('basic_info2_item_template')); ?>" class="item_templink_a text-dark">
    <button class=" btn-block item_templink shadow-sm mr-md-5 mt-md-4 bg-white d-flex flex-row align-items-center">
<div class="pl-2 mb-0 text-danger text-center bg-white"><i class="fab fa-adobe m-0"></i></div>
<div class="flex-fill  p-3 text-center font-weight-bold"><span>テンプレートをダウンロード</span>
</div>
</button>
<div class="cart_btn_wrapper_top mt-5 mt-md-3">
    <a class="btn cart_btn btn-lg btn-block" href="<?php (the_field('basic_info2_carturl')); ?>" target="_b" roll="button"><i class="fas fa-shopping-cart"></i> カートに入れる</a>
<?php if(wp_is_mobile()):?>
<div class="mt-3">
    <a class=""><i class="fas fa-arrow-right"></i> 大口のご注文をご希望の方はこちら</a>
</div>
<?php endif;?>
</div>
</a>
