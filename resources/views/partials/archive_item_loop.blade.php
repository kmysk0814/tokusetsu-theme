<!-- ----------------　-->
<!-- 商品一覧ページの各ループ　-->
<!-- ----------------　-->
<?php
// カテゴリを全て取得
$category = get_the_category();
// カテゴリIDを取得
$category_id = $category[0]->cat_ID;
// カテゴリ名を取得
$category_name = $category[0]->cat_name;
// スラッグを取得
$category_slug = $category[0]->category_nicename;
// カテゴリへのリンクを取得
$category_link = get_category_link($category_id);
?>


<article @php post_class() @endphp>
<div class="item-archive-flexitem">
  <header class="item-archive-header">

  	<a class="item-archive-imagelink" href="<?php the_permalink(); ?>">
	<img src="<?php (the_field('basic_item_grp_img_item_image1')); ?>" alt="<?php (the_field('basic_item_grp_img_alt')); ?>" class="">
	</a>
    <h2 class="font-weight-bold mb-1 entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h2>
    <!-- リンク出力 -->
	<p class="badge badge-secondary shadow-sm text-white item-archive-cat">
	<!-- リンク文字を出力 -->
	<span>
	<?php echo $category_name; ?>
	</span></p>

	<div class="entry-summary">
    <!-- @php the_excerpt() @endphp -->

	<p class="item_archive_price">
	<?php (the_field('basic_info2_item_grp_price_item_number_min_lot')); ?>
	<?php (the_field('basic_info2_item_grp_price_item_number_item_unit')); ?> 
	税込
	<?php (the_field('basic_info2_item_grp_price_item_price')); ?>
	円〜
	</p>
　　</div>

  </header>

</div>
</article>
