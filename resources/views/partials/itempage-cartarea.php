<section class="cart_wrapper">
    <div class="row">
        <div class="col-12 col-md-12 cart_container">
            <div class="row p-md-5">
                <div class="col-5">
                    <?php
                        $rows = get_field('item_images' ); // すべてのrow（内容・行）をいったん取得する
                        $first_row = $rows[0]; // 1行目だけを$first_rowに格納しますよ～
                        $first_row_image = $first_row['item_image']; // get the sub field value
                        ?>
                    <img src="<? echo $first_row_image ?>" alt="<? echo $first_row['alt'] ?>" class="item_image">
                    </div>
                    <div class="col-7">
                        <h5 class="item_title font-weight-bold pb-2">
                            <?php (the_field('basic__item_name')); ?>
                        </h5>
                        <p class="mt-md-3">
                            <?php (the_field('basic_info2_item_grp_price_item_number_min_lot')); ?>
                            <?php (the_field('basic_info2_item_grp_price_item_number_item_unit')); ?>
                            税込
                            <?php (the_field('basic_info2_item_grp_price_item_price')); ?>
                            円〜
                        </p>
                        <?php if(!wp_is_mobile()):?>
                        <div class="cart_btn_wrapper mb-md-3">
                            <a class="btn cart_btn btn-lg btn-block" href="<?php (the_field('basic_info2_carturl')); ?>" target="_b" roll="button"><i class="fas fa-shopping-cart"></i> カートに入れる</a>
                        </div>
                        <a>→大口のご注文をご希望の方はこちら</a>
                        <?php else:?>
                        <?php endif;?>
                    </div>
                    <?php if(wp_is_mobile()):?>
                    <div class="col-12">
                        <div class="cart_btn_wrapper mt-4 mb-3">
                            <a class="btn cart_btn btn-lg btn-block" href="<?php (the_field('basic_info2_carturl')); ?>" roll="button"><i class="fas fa-shopping-cart"></i> カートに入れる</a>
                        </div>
                        <a>→大口のご注文をご希望の方はこちら</a>
                    </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
</section>
