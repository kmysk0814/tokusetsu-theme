
<!-- ----------------　-->
<!-- 新着情報ページの各ループ　-->
<!-- ----------------　-->

<li class="list-group-item news-list-group">

<span class="small text-muted">
<?php echo get_the_date(); ?>
</span>

<?php if( empty(get_the_content()) ) : // 本文を空白にした場合はリンクなし ?>
<p><?php echo get_the_title(); ?></p>
<?php elseif( empty(get_the_title()) ) : // タイトルが空白の場合は本文をタイトルとして表示 ?>
<?php remove_filter('the_content', 'wpautop'); ?><?php the_content(); ?>
<?php else : // 通常の投稿 ?>
<p><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></p>
<?php endif; ?>



</li>