<div class="item_tab_menu px-md-5">
    <!-- カプセル・メニュー -->
    <ul id="item_tab" class="nav nav-pills nav-justified">
        <li class="mr-3 my-3">
            <a class="btn btn-outline-dark btn-large noScroll" href="#siyouhyou" data-toggle="tab"><i class="fas fa-sort-down"></i>仕様表</a>
        </li>
        <li class="mr-3 my-3 active">
            <a class="btn btn-outline-dark btn-large noScroll" href="#option" data-toggle="tab"><i class="fas fa-sort-down"></i>印刷・オプションについて</a>
        </li>
        <li class="mr-3 my-3">
            <a class="btn btn-outline-dark btn-large noScroll" href="#howtomake" data-toggle="tab"><i class="fas fa-sort-down"></i>データ作成ガイド</a>
        </li>
    </ul>
    <!-- 内容 -->
    <div class="tab-content">
        <div class="fade tab-pane" id="siyouhyou">
            <div class="border  p-4">
                <?
                $table = get_field( 'item_siyou' );
                if ( ! empty ( $table ) ) {
                    echo '<table  class="table table-responsive table-striped">';
                    if ( ! empty( $table['caption'] ) ) {
                        echo '<caption>' . $table['caption'] . '</caption>';
                    }
                    if ( ! empty( $table['header'] ) ) {
                        echo '<thead class="thead-light">';
                        echo '<tr>';
                        foreach ( $table['header'] as $th ) {
                            echo '<th>';
                            echo $th['c'];
                            echo '</th>';
                        }
                        echo '</tr>';
                        echo '</thead>';
                    }
                    echo '<tbody>';
                    foreach ( $table['body'] as $tr ) {
                        echo '<tr>';
                        foreach ( $tr as $td ) {
                            echo '<td>';
                            echo $td['c'];
                            echo '</td>';
                        }
                        echo '</tr>';
                    }
                    echo '</tbody>';
                    echo '</table>';
                }
                ?>
            </div>
        </div>
        <div class="fade tab-pane show active" id="option">
            <div class="border  px-4">
                <?
                $option = get_field('option');
                if (!empty($option)) echo $option; ?>
            </div>
        </div>
        <div class="fade tab-pane" id="howtomake">
            @include('components.dataguide')
        </div>
    </div>
</div>
<!-- item_tab_menu -->
