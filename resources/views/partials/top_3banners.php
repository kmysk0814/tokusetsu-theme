<?php if(!wp_is_mobile()): ?>
<!--
   <div class="recommend_bg">
    <section class="cont_wrap pt-md-5 reccomend_section">
        <div class="d-flex justify-content-center mb-md-5">
            <div class="card mx-2">
                <a href="<?php the_field('banner1_link',69); ?>"><img class="card-img" src="<?php the_field('banner1_image',69); ?>" alt="<?php the_field('banner1_alt',69); ?>"></a>
                <div class="card-img-overlay">
                    <h5 class="card-title"><a href="<?php the_field('banner1_link',69); ?>" class=""><?php the_field('banner1_title',69); ?></a></h5>
                    <p class="card-text"><a href="<?php the_field('banner1_link',69); ?>" class=""><?php the_field('banner1_disc',69); ?></a></p>
                </div>
            </div>
            <div class="card mx-2">
                <a href="<?php the_field('banner2_link',69); ?>"><img class="card-img" src="<?php the_field('banner2_image',69); ?>" alt="<?php the_field('banner2_alt',69); ?>"></a>
                <div class="card-img-overlay">
                    <h5 class="card-title"><?php the_field('left_title',69); ?></h5>
                    <p class="card-text"><a href="<?php the_field('banner2_link',69); ?>" class=""><?php the_field('banner2_disc',69); ?></a></p>
                </div>
            </div>
            <div class="card mx-2">
                <a href="<?php the_field('banner3_link',69); ?>"><img class="card-img" src="<?php the_field('banner3_image',69); ?>" alt="<?php the_field('banner3_alt',69); ?>"></a>
                <div class="card-img-overlay">
                    <h5 class="card-title"><?php the_field('left_title',69); ?></h5>
                    <p class="card-text"><a href="<?php the_field('banner3_link',69); ?>" class=""><?php the_field('banner3_disc',69); ?></a></p>
                </div>
            </div>
        </div>
    </section>
</div>
-->
<div class="recommend_bg oveflow-hidden">
    <section class="cont_wrap pt-md-5 reccomend_section">
        <div class="swiper-container swiper-desk">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <a href="<?php the_field('banner1_link',69); ?>"><img class="card-img" src="<?php the_field('banner1_image',69); ?>" alt="<?php the_field('banner1_alt',69); ?>"></a>
                    <div class="swiper-slide-content swiper-slide-content-3">
                        <h5 class="card-title"><a href="<?php the_field('banner1_link',69); ?>" class=""><?php the_field('banner1_title',69); ?></a></h5>
                        <p class="card-text"><a href="<?php the_field('banner1_link',69); ?>" class=""><?php the_field('banner1_disc',69); ?></a></p>
                    </div>
                </div>
                <div class="swiper-slide">
                    <a href="<?php the_field('banner2_link',69); ?>"><img class="card-img" src="<?php the_field('banner2_image',69); ?>" alt="<?php the_field('banner2_alt',69); ?>"></a>
                    <div class="swiper-slide-content swiper-slide-content-3">
                        <h5 class="card-title"><a href="<?php the_field('banner2_link',69); ?>" class=""><?php the_field('banner2_title',69); ?></a></h5>
                        <p class="card-text"><a href="<?php the_field('banner2_link',69); ?>" class=""><?php the_field('banner2_disc',69); ?></a></p>
                    </div>
                </div>
                <div class="swiper-slide">
                    <a href="<?php the_field('banner3_link',69); ?>"><img class="card-img" src="<?php the_field('banner3_image',69); ?>" alt="<?php the_field('banner3_alt',69); ?>"></a>
                    <div class="swiper-slide-content swiper-slide-content-3">
                        <h5 class="card-title"><a href="<?php the_field('banner3_link',69); ?>" class=""><?php the_field('banner3_title',69); ?></a></h5>
                        <p class="card-text"><a href="<?php the_field('banner3_link',69); ?>" class=""><?php the_field('banner3_disc',69); ?></a></p>
                    </div>
                </div>
            </div>
            <!-- If we need navigation buttons -->
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
    </section>
</div>
<?php else:?>
<div class="recommend_bg_m">
    <div class="swiper-mobile">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <a href="<?php the_field('banner1_link',69); ?>"><img class="card-img" src="<?php the_field('banner1_image',69); ?>" alt="<?php the_field('banner1_alt',69); ?>"></a>
                <div class="swiper-slide-content swiper-slide-content-3">
                    <h5 class="card-title"><a href="<?php the_field('banner1_link',69); ?>" class=""><?php the_field('banner1_title',69); ?></a></h5>
                    <p class="card-text"><a href="<?php the_field('banner1_link',69); ?>" class=""><?php the_field('banner1_disc',69); ?></a></p>
                </div>
            </div>
            <div class="swiper-slide">
                <a href="<?php the_field('banner2_link',69); ?>"><img class="card-img" src="<?php the_field('banner2_image',69); ?>" alt="<?php the_field('banner2_alt',69); ?>"></a>
                <div class="swiper-slide-content swiper-slide-content-3">
                    <h5 class="card-title"><?php the_field('left_title',69); ?></h5>
                    <p class="card-text"><a href="<?php the_field('banner2_link',69); ?>" class=""><?php the_field('banner2_disc',69); ?></a></p>
                </div>
            </div>
            <div class="swiper-slide">
                <a href="<?php the_field('banner3_link',69); ?>"><img class="card-img" src="<?php the_field('banner3_image',69); ?>" alt="<?php the_field('banner3_alt',69); ?>"></a>
                <div class="swiper-slide-content swiper-slide-content-3">
                    <h5 class="card-title"><?php the_field('left_title',69); ?></h5>
                    <p class="card-text"><a href="<?php the_field('banner3_link',69); ?>" class=""><?php the_field('banner3_disc',69); ?></a></p>
                </div>
            </div>
        </div>
        <div class="swiper-pagination"></div>
    </div>
</div>
<?php endif;?>
