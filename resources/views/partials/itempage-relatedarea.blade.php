<section class="related_item">
    <div class="related_wrapper">
        <div class="related_content">
            <?php
            $posts = get_field('related_item');
            if( $posts ):
            ?>
            <div class="text-center">
                <h3 class="related_head head_news">オススメの商品</h3>
            </div>
            <ul>
                <?php foreach( $posts as $val ): ?>
                <li>
                    <div class="mb-3">
                        <?  $brand = get_field('item_images',$val);
                        $src = $brand[0]['item_image'];
                        ?>
                        <a href="<?php echo get_permalink( $val->ID ); ?>" class="related_link">
                            <img src="<?php echo $src; ?>" alt="<?php (the_sub_field('alt')); ?>" class="">
                        </a>
                    </div>
                    <a href="<?php echo get_permalink( $val->ID ); ?>" class="related_link"><?php echo get_the_title( $val->ID ); ?></a>
                </li>
                <?php endforeach; ?>
            </ul>
            <?php endif; ?>
        </div>
    </div>
</section>
