<div id="category_modal">
    <div class="category_modal_cont d-flex">
        <?php
$defaults = array(
'menu'            => '3',
'menu_class'      => 'menu',
'container'       => 'div',
'container_class' => '',
'container_id'    => '',
'fallback_cb'     => 'wp_page_menu',
'before'          => '',
'after'           => '',
'link_before'     => '',
'link_after'      => '',
'echo'            => true,
'depth'           => 0,
'walker'          => '',
'theme_location'  => '',
'items_wrap'      => '<ul id="category_modal_ul" class="d-flex justify-content-start flex-wrap flex-column p-0 list-unstyled">%3$s</ul>',
);
//wp_nav_menu( $defaults );
?>
        <?php
    $categories = get_categories();
    //$categories = get_categories('parent=0'); 親カテゴリーのみ
    foreach($categories as $category) :
    $cat_id = $category->cat_ID;
    $post_id = 'category_'.$cat_id;
    $catimg = get_field('category_image',$post_id);
    $img = wp_get_attachment_image_src($catimg, 'thumbnail');
    ?>
        <div class="category_wrap">
            <div><a href="<?php echo esc_url( home_url( '/' ) ); ?>
category/<?php echo $category->slug; ?>"><img class="cat_thumb" src="<?php echo $catimg; ?>" alt="<?php echo $category->cat_name; ?>" /></a></div>
            <div><?php echo $category->cat_name; ?></div>
        </div>
        <?php endforeach; ?>
    </div>
</div>
