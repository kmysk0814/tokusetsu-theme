<h3 class="item_para font-weight-bold text-white bg-dark p-3 mt-5 mb-2">
    <i class="fas fa-bookmark"></i>　価格とお届け日について
</h3>
<div class="item_price_table px-3 px-md-5 my-3">
    <?php if(have_rows('price_table_field')): ?>
    <?php while(have_rows('price_table_field')): the_row(); ?>
    <?php
    $table = get_sub_field( 'price_table' );
    if ( ! empty ( $table ) ) {
        echo '<table  class="table table-responsive table-striped mb-5">';
        if ( ! empty( $table['caption'] ) ) {
            echo '<caption>' . $table['caption'] . '</caption>';
        }
        if ( ! empty( $table['header'] ) ) {
            echo '<thead class="thead-light">';
            echo '<tr>';
            foreach ( $table['header'] as $th ) {
                echo '<th>';
                echo $th['c'];
                echo '</th>';
            }
            echo '</tr>';
            echo '</thead>';
        }
        echo '<tbody>';
        foreach ( $table['body'] as $tr ) {
            echo '<tr>';
            foreach ( $tr as $td ) {
                echo '<td>';
                echo $td['c'];
                echo '</td>';
            }
            echo '</tr>';
        }
        echo '</tbody>';
        echo '</table>';
        $price_foot = get_sub_field( 'price_foot' );
        if ( ! empty ( $price_foot ) ) {
            echo '<p class="mb-5"><small>' .nl2br($price_foot) .'</small></p>';
        };
    }
    ?>
    <?php endwhile; ?>
    <?php endif; ?>



</div>
