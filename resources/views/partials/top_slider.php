<section class="slider_padding">
    <?php if(!wp_is_mobile()): ?>
    <div class="row heaer_img">
        <div class="col-sm-12 p-0">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <!--
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
-->
                <div class="carousel-inner">
                    <div class="carousel-item active" data-interval="10000">
                        <a href="<?php the_field('link1',30); ?>">
                          <img class="d-block w-100" src="<?php the_field('image1',30); ?>" alt="<?php the_field('alt1',30); ?>">
                          </a>
                    </div>
                </div>
            </div>
        </div>
        <?php else: ?>
        <div class="row">
            <div class="mobile_header col-xs-12 px-0">
                <img width="100%" src="<?php echo esc_url( home_url( '/' ) ); ?>
wp-content/uploads/2020/01/ac_top_mobile_0114.jpg" alt="">
        </div>
            </div>
            <?php endif; ?>
        </div>
</section>
