<section class="pickup_section p-md-5">
    <div class="cont_wrap">
        <div class="text-center">
            <h2 class="mt-5 head_news">注目ランキング</h2>
        </div>
        <?php
// 条件の設定
$args = [
    'post_type' => 'item', // 検索したいカスタム投稿タイプ
    'tax_query' => [
        [
            'taxonomy' => 'pickup', // 絞り込みたいカスタムタクソノミー
            'field' => 'name', // スラッグで検索。idでも検索できます
            'terms' => array('1','2','3','4','5'), // 検索する文字列（fieldがidならidが入ります）
            'include_children' => true, // 子タクソノミーを含むかどうか
            'operator' => 'IN' // termsが複数ある場合AND検索（全て）かIN検索（いずれか）かNOT IN（いずれも除く）
        ]
    ],
    'post_count' => 5,
    'paged'      => get_query_var('paged') ? intval(get_query_var('paged')) : 1,
];

$the_query = new WP_Query($args);
if($the_query->have_posts()):
?>
        <div class="pickup_wrapper">
            <?php while ($the_query->have_posts()): $the_query->the_post(); ?>
<?php
    $rows = get_field('item_images' ); // すべてのrow（内容・行）をいったん取得する
    $first_row = $rows[0]; // 1行目だけを$first_rowに格納しますよ～
    $first_row_image = $first_row['item_image']; // get the sub field value
    $imagesrc = wp_get_attachment_image_src( $first_row_image, 'full' );
?>

            <div id="post-<?php the_ID(); ?>" <?php post_class('pickup-item'); ?>>
                <div class="row mx-1 pickup-item-content">
                    <div class="col-sm-12 col-md-12 col-lg-12 px-0">
                        <div class="card picklist">
                            <div class="img-container">
                                <a href="<?php the_permalink(); ?>" class="item-list-eyecatch">
                                    <img src="<? echo $first_row_image ?>" alt="<? echo $first_row['alt'] ?>" class="card-img-top">
                                </a>
                            </div>
                        </div>
                        <div class="pickup_item_disc py-3 text-center">
                            <h5 class="card-title small">
                                <a class="text-dark" href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a>
                            </h5>
                            <!-- 値段 -->
                            <div class="item-list-price">
                                <?php echo $value = get_post_meta( get_the_ID(), '値段', true ); ?>
                            </div>
                            <?php
                            // カテゴリを全て取得
                            $category = get_the_category();
                            // カテゴリIDを取得
                            $category_id = $category[0]->cat_ID;
                            // カテゴリ名を取得
                            $category_name = $category[0]->cat_name;
                            // スラッグを取得
                            $category_slug = $category[0]->category_nicename;
                            // カテゴリへのリンクを取得
                            $category_link = get_category_link($category_id);
                            ?>
                            <!-- リンク出力 -->
                            <p class="post-category">
                                <a href="<?php echo $category_link; ?>">
                                <!-- リンク文字を出力 -->
                                <?php echo $category_name; ?></a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
            <?php else: ?>
            <p>まだ投稿がありません。</p>
            <?php endif; ?>
        </div>
    </div>
</section>
