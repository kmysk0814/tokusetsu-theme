<div class="swiper-background">
<!-- スライダー全体を括るメインコンテナ -->
<div class="swiper-item">
<!-- 全スライドをまとめるラッパー -->
<div class="swiper-wrapper">
<!-- 各スライド -->
<?php if(have_rows('item_images')): ?>
<? $num =0 ?>
<?php while(have_rows('item_images')): the_row(); ?>
<div class="swiper-slide slide1">
<!--<a href="#lightbox" data-toggle="modal" data-slide-to="<? //echo $num ?>">-->
<img src="<?php (the_sub_field('item_image')); ?>" alt="<?php (the_sub_field('alt')); ?>" class="">
<!--</a>-->
</div>
<? $num +=1 ?>
<?php endwhile; ?>
<?php endif; ?>
</div>
</div>
</div>
<!--— サムネイル ---->
<ul class="thumb-list">
<?php if(have_rows('item_images')): ?>
<? $n = 0; ?>
<?php while(have_rows('item_images')): the_row(); ?>
<li class="thumb-item">
<a class="thumb-link one" href="javascript:void(0);" onclick="slideThumb(<?echo $n?>)">
<img src="<?php (the_sub_field('item_image')); ?>" alt="<?php (the_sub_field('alt')); ?>" class="w-100">
</a>
</li>
<? $n +=1; ?>
<?php endwhile; ?>
<?php endif; ?>
</ul>
