<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
<!--bootstrap-->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<!--styles-->
<link rel="stylesheet" id="sage/main.css-css" href="<?php echo esc_url( home_url( '/' ) ); ?>
wp-content/themes/tokusetsu-
theme/resources/assets/styles/style.css" type="text/css" media="all">
<link rel="stylesheet" href="<?php echo esc_url( home_url( '/' ) ); ?>
wp-content/themes/tokusetsu-theme/resources/assets/styles/item_page_component.css" type="text/css" media="all">
<!--styles responsive-->
    <link rel="stylesheet" href="<?php echo esc_url( home_url( '/' ) ); ?>
wp-content/themes/tokusetsu-theme/resources/assets/styles/tablet.css" type="text/css" media="screen and (min-width: 601px) and (max-width: 1280px)">
    <link rel="stylesheet" href="<?php echo esc_url( home_url( '/' ) ); ?>
wp-content/themes/tokusetsu-theme/resources/assets/styles/sp.css" type="text/css" media="screen and (max-width: 600px)">
<!--plugins-->
<link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">

@php wp_head() @endphp

<!--fonts-->
<script src="https://kit.fontawesome.com/5f712b6527.js" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,700&display=swap" rel="stylesheet">
<!-- Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>
    (function(d) {
        var config = {
                kitId: 'khg0jek',
                scriptTimeout: 3000,
                async: true
            },
            h = d.documentElement,
            t = setTimeout(function() {
                h.className = h.className.replace(/\bwf-loading\b/g, "") + " wf-inactive";
            }, config.scriptTimeout),
            tk = d.createElement("script"),
            f = false,
            s = d.getElementsByTagName("script")[0],
            a;
        h.className += " wf-loading";
        tk.src = 'https://use.typekit.net/' + config.kitId + '.js';
        tk.async = true;
        tk.onload = tk.onreadystatechange = function() {
            a = this.readyState;
            if (f || a && a != "complete" && a != "loaded") return;
            f = true;
            clearTimeout(t);
            try {
                Typekit.load(config)
            } catch (e) {}
        };
        s.parentNode.insertBefore(tk, s)
    })(document);
</script>
</head>
