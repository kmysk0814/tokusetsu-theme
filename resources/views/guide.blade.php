<?php
/*
Template Name: ご利用ガイド
*/
?>


@extends('layouts.app')

@section('content')
  @include('partials.page-header')

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif



<img src="<?php echo esc_url( home_url( '/' ) ); ?>
wp-content/uploads/2019/11/guid_cmp-scaled.png" class="guide_cmp" alt="">



  {!! get_the_posts_navigation() !!}
@endsection
