<!-- ----------------　-->
<!-- 商品一覧ページの外枠　-->
<!-- ----------------　-->
@extends('layouts.app')
@section('content')
@include('partials.page-header')
<div class="slider_padding">
    <section class="category_head_image mb-md-5" style="background-image:url('<?php echo esc_url( home_url( '/' ) ); ?>
wp-content/uploads/2019/11/back_news.png')">
        <div class="sp_head_wrap cont_wrap category_head_wrap">
            <h1 class="h3 category_head font-weight-bold text-center my-md-5">
                新着情報</h1>
        </div>
    </section>
    <div class="container news_archive">
        <div class="row">
            <div class="col-md-3">
                @include('components.side-menu')
            </div>
            <div class="col-md-9">
                @if (!have_posts())
                <div class="alert alert-warning">

                    {{ __('Sorry, no results were found.', 'sage') }}
                </div>
                {!! get_search_form(false) !!}
                @endif
                <div id="news-archive-grid" class="shadow-sm">
                    <ul class="list-group list-group-flush">
                        @while (have_posts()) @php the_post() @endphp
                        @include('partials.archive_news_loop')
                        @endwhile
                    </ul>
                </div>
                <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
            </div>
        </div>
    </div>
    @endsection
