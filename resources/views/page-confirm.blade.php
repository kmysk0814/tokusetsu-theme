<?php
/**
 * Template Name: ConfirmPage
 */
?>
@extends('layouts.app')
@section('content')
@include('partials.page-header')

<?php echo do_shortcode("[formPhp file='confirm']"); ?>

@endsection

