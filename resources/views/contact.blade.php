<?php
/*
Template Name: コンタクトページ
*/
?>


@extends('layouts.app')

@section('content')
  @include('partials.page-header')

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

<div class="slider_padding">
<h1 class="text-center">Contact</h1>
<?php echo do_shortcode('[contact-form-7 id="147" title="トップページのフォーム"]
'); ?>
</div>








  {!! get_the_posts_navigation() !!}
@endsection
