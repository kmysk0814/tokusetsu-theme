<!-- ----------------　-->
<!-- カテゴリページの外枠　-->
<!-- ----------------　-->
<?php
    $current_cat = get_queried_object();
    $cat_slug = $current_cat->slug;
    $cat_name = $current_cat->name;

//ヘッダー画像url取得
    $cat_id = $current_cat->cat_ID; // カテゴリのIDを取得
    $post_id = 'category_'. $cat_id; // カテゴリIDを付けて、category_1 の様な形にする
    $cat_imgid = get_field('category_header', $post_id); // cat_imgはフィールド名を入力
$cat_head = get_field('category_head', $post_id); // cat_imgはフィールド名を入力
$cat_disc = get_field('category_disc', $post_id); // cat_imgはフィールド名を入力
?>
@extends('layouts.app')
@section('content')
@include('partials.page-header')
<div class="slider_padding">
    <section class="category_head_image mb-md-5" style="background-image:url('<? echo $cat_imgid ?>')">
        <div class="sp_head_wrap cont_wrap category_head_wrap">
            <h1 class="h3 category_head font-weight-bold text-center my-md-5">

                {{ $cat_name }}</h1>
        </div>
    </section>
    <div class="container category_item_wrap">
        <div class="col-md-9 cat_caption">
            <div class="row mb-md-5">
                <div class="category_head_text">
                    <h2 class="h3 mb-md-3">
                        <? echo $cat_head ?>
                    </h2>
                    <p>
                        <? echo $cat_disc ?>
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                @include('components.side-menu')
            </div>
            <div class="col-md-9">
                <?php
    $args = array(
    'post_type' => 'item', //投稿を表示
    'posts_per_page' => 0, //表示する件数
    'category_name' => $cat_slug,
    );
    $the_query = new WP_Query( $args );

    if ( !$the_query->have_posts() ) :?>
                <div class="alert alert-warning">

                    {{ __('Sorry, no results were found.', 'sage') }}
                </div>
                {!! get_search_form(false) !!}
                <?php endif; ?>
                <div id="item-archive-grid">
                    <div class="d-flex flex-row align-items-stretch flex-wrap justify-content-start">
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        @include('partials.archive_category_loop')
                        <?php endwhile; ?>
                    </div>
                </div>
                <?php wp_reset_query(); ?>
            </div>
        </div>
    </div>
</div>
@endsection
