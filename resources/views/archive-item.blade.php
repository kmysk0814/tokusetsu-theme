<!-- ----------------　-->
<!-- 商品一覧ページの外枠　-->
<!-- ----------------　-->
@extends('layouts.app')
@section('content')
@include('partials.page-header')
<div class="container slider_padding">
    <h1 class="font-weight-bold text-center my-5">商品一覧</h1>
    <div class="row">
        <div class="col-xs-12">
            @if (!have_posts())
            <div class="alert alert-warning">

                {{ __('Sorry, no results were found.', 'sage') }}
            </div>
            {!! get_search_form(false) !!}
            @endif
            <div id="item-archive-grid">
                <div class="d-flex flex-row align-items-stretch flex-wrap justify-content-start">
                    @while (have_posts()) @php the_post() @endphp
                    @include('partials.archive_item_loop')
                    @endwhile
                </div>
            </div>
            <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
        </div>
    </div>
</div>
@endsection
