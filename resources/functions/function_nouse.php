<?php

// ------------------------------------------
// 整理しましょう
// 使っていない可能性が高いで
//
//
// ピックアップ・ランキング部分の為に
// メニューの出力内容をカスタム
// ------------------------------------------


//メニューの出力内容をカスタム
class Walker_Nav_Menu_Custom extends Walker_Nav_Menu {
function start_el(&$output, $item, $depth = 0, $args = Array(), $id = 0) {
    $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
    $classes = empty( $item->classes ) ? array() : ( array )$item->classes;
    $classes[] = 'menu-item-' . $item->ID;
    $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
    // 不要なIDを削除してli要素に任意のクラスをつける
    $id = apply_filters( 'nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args, $depth );
    $id = $id ? ' id="' . esc_attr( $id ) . '"': '';
    $class_names = $class_names ? ' class="archive"' : '';
    $output .= $indent . '<li' . $class_names . '>';
    $atts = array();
    $atts[ 'title' ] = !empty( $item->attr_title ) ? $item->attr_title : '';
    $atts[ 'target' ] = !empty( $item->target ) ? $item->target : '';
    $atts[ 'rel' ] = !empty( $item->xfn ) ? $item->xfn : '';
    $atts[ 'href' ] = !empty( $item->url ) ? $item->url : '';
    $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );
    $attributes = '';
    foreach ( $atts as $attr => $value ) {
      if ( !empty( $value ) ) {
        $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
        $attributes .= ' ' . $attr . '="' . $value . '"';
      }
    }
    $item_output = $args->before;
    $item_output .= '<a' . $attributes . '>';

    //サムネイル画像を追加・画像にclass[thumnail-img]を付与
    $item_output .= '<figure>';
    $item_output .= get_the_post_thumbnail( $item->object_id, 'thumbnail', array( 'class' => 'thumbnail-img' ), array( 'alt' => $item->title ) );
    $item_output .= '</figure>';

    //日付・カテゴリーを<div class="span-wrap">で囲って出力
    $item_output .= '<div class="span-wrap">';
    $cat = get_the_category( $item->object_id );
    $catName = $cat[ 0 ]->name;
    $item_output .= '<span class="cat">' . $catName . '</span>';
    $item_output .= '<span class="date">' . get_the_date( 'Y/m/d', $item->object_id ) . '</span>';
    $item_output .= '</div>';

    //タイトルを表示
    $item_output .= '<p class="title">' . $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after . '</p>';

    //本文の抜粋を表示
    $item_output .= '<p class="text">';
    $post = get_post( $item->object_id );
    $item_output .= mb_substr( strip_tags( $post->post_content ), 0, 50 );
    $item_output .= '...</p>';

    $item_output .= '</a>';
    $item_output .= $args->after;
    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }
}


// ------------------------------------------
// カスタム投稿 > トップスライダー　に投稿された画像の
// 情報を収集します。
// トップスライダー本体の記述ファイルは [topslider.php] です。
// ------------------------------------------

function retrieve_images_in_post( $post_id=null, $image_type=null ) {
  $image_count = 0;
  $images = [
    'thumbnails' => [], 
    'attachments' => [], 
    'embeds' => []
  ];

  // Initialize by dynamically allocating an argument
  $arguments = func_get_args();
  if (array_key_exists(strtolower(end($arguments)), $images)) {
    $image_type = array_pop($arguments);
  } else {
    $image_type = null;
  }
  $post_id = isset($arguments[0]) && intval($arguments[0]) > 0 ? intval($arguments[0]) : null;

  if (empty($post_id)) {
    global $wp_query;
    $_post = $wp_query->post;
    $post_id = isset($_post->ID) ? $_post->ID : false;
    if (!$post_id) 
      return false;
  }

  $post = get_post($post_id);
  if (empty($post)) 
    return false;

  // リンクurlを取得
  if (preg_match_all('/<a.*href=(|["\'])(.*)\1*[^>]*>/iU', $post->post_content, $matches) && is_array($matches) && array_key_exists(0, $matches)) {
    foreach ($matches[0] as $candidate) {
      if (preg_match('/(?<!_)href=([\'"])?(.*?)\\1/i', $candidate, $matches) && is_array($matches) && array_key_exists(2, $matches)) {
        $image_path = $matches[2];
      } else
      if (preg_match('/(?<!_)href=\s?(.*?)\s*([^>]*)>/i', $candidate, $matches)) {
        $image_path = strstr($matches[2], ' ', true);
      }
      if (isset($image_path) && !empty($image_path)) {
        if (strpos( $image_path, 'http' ) === false) {
          $images['links'][] = file_exists(ABSPATH . $image_path) ? site_url($image_path) : $image_path;
        } else {
          $images['links'][] = $image_path;
        }
      }
    }
    $images['links'] = array_unique($images['links']);
  }

  // 画像urlを取得
  if (preg_match_all('/<img.*src=(|["\'])(.*)\1*[^>]*>/iU', $post->post_content, $matches) && is_array($matches) && array_key_exists(0, $matches)) {
    foreach ($matches[0] as $candidate) {
      if (preg_match('/(?<!_)src=([\'"])?(.*?)\\1/i', $candidate, $matches) && is_array($matches) && array_key_exists(2, $matches)) {
        $image_path = $matches[2];
      } else
      if (preg_match('/(?<!_)src=\s?(.*?)\s*([^>]*)>/i', $candidate, $matches)) {
        $image_path = strstr($matches[2], ' ', true);
      }
      if (isset($image_path) && !empty($image_path)) {
        if (strpos( $image_path, 'http' ) === false) {
          $images['imageurls'][] = file_exists(ABSPATH . $image_path) ? site_url($image_path) : $image_path;
        } else {
          $images['imageurls'][] = $image_path;
        }
      }
    }
    $images['imageurls'] = array_unique($images['imageurls']);
  }

  foreach ($images as $_type => $_images) {
    $_count = count($_images);
    if (!empty($image_type) && $_type === $image_type) {
      return $_count > 0 ? $_images : false;
    }
    $image_count += $_count;
  }

  return $image_count === 0 ? false : $images;

}

// 自動整形をきる

remove_filter( 'the_content', 'wpautop' );
function override_mce_options( $init_array ) {
    $init_array['indent']  = true;
    $init_array['wpautop'] = false;

    return $init_array;
}

add_filter( 'tiny_mce_before_init', 'override_mce_options' );

?>