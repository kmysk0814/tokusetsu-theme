<?php


// ------------------------------------------
// カスタム投稿タイプを増やすコード
// ダッシュボードに追加されます 編集はプラグイン：CPTUIを使用
// ------------------------------------------

function cptui_register_my_cpts() {

  /**
   * Post Type: sliders.
   */

  $labels = [
    "name" => __( "トップスライダー", "sage" ),
    "singular_name" => __( "slider", "sage" ),
    "menu_name" => __( "スライダーを編集", "sage" ),
    "all_items" => __( "スライダーの編集", "sage" ),
    "edit_item" => __( "登録バナー画像を選択する", "sage" ),
    ];

  $args = [
    "label" => __( "sliders", "sage" ),
    "labels" => $labels,
    "description" => "test",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "delete_with_user" => false,
    "show_in_rest" => true,
    "rest_base" => "",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => false,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "delete_with_user" => false,
    "exclude_from_search" => false,
    "capability_type" => "post",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "menu_position" => 3,
    "menu_icon" => "dashicons-format-gallery",
    "rewrite" => [ "slug" => "slider", "with_front" => true ],
    "query_var" => true,
    "supports" => [ "title", "editor", "thumbnail", "custom-fields", "revisions" ],
  ];

  register_post_type( "sliders", $args );

  /**
   * Post Type: news.
   */

  $labels = [
    "name" => __( "新着情報", "sage" ),
    "singular_name" => __( "news", "sage" ),
    "menu_name" => __( "新着情報を追加", "sage" ),
    "all_items" => __( "過去の投稿を編集", "sage" ),
    "add_new" => __( "新しい記事を追加", "sage" ),
    "add_new_item" => __( "新しい記事を執筆中", "sage" ),
    "edit_item" => __( "過去の投稿を編集中", "sage" ),

    ];

  $args = [
    "label" => __( "news", "sage" ),
    "labels" => $labels,
    "description" => "test",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "delete_with_user" => false,
    "show_in_rest" => true,
    "rest_base" => "",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => true,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "delete_with_user" => false,
    "exclude_from_search" => false,
    "capability_type" => "post",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "menu_position" => 3,
    "menu_icon" => "dashicons-format-status",
    "rewrite" => [ "slug" => "news", "with_front" => true ],
    "query_var" => true,
    "supports" => [ "title", "editor", "thumbnail", "custom-fields", "revisions" ],
  ];

  register_post_type( "news", $args );

  /**
   * Post Type: footer.
   */

  $labels = [
    "name" => __( "フッターを編集", "sage" ),
    "singular_name" => __( "footer", "sage" ),
    "menu_name" => __( "フッターを編集", "sage" ),
    "all_items" => __( "フッターを編集", "sage" ),
    "add_new" => __( "フッターブロックを追加(php編集が伴います)", "sage" ),
    "add_new_item" => __( "フッターブロックを追加中", "sage" ),
    "edit_item" => __( "フッター内容を編集中", "sage" ),

    ];

  $args = [
    "label" => __( "footer", "sage" ),
    "labels" => $labels,
    "description" => "フッターの内容を記述します",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "delete_with_user" => false,
    "show_in_rest" => true,
    "rest_base" => "",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => true,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "delete_with_user" => false,
    "exclude_from_search" => false,
    "capability_type" => "post",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "menu_position" => 3,
    "menu_icon" => "dashicons-format-status",
    "rewrite" => [ "slug" => "footer", "with_front" => true ],
    "query_var" => true,
    "supports" => [ "title", "editor", "thumbnail", "custom-fields", "revisions" ],
  ];

  register_post_type( "footer", $args );

}

add_action( 'init', 'cptui_register_my_cpts' );

function cptui_register_my_taxes_pickup() {

  /**
   * Taxonomy: ピックアップ.
   */

  $labels = [
    "name" => __( "ピックアップ", "sage" ),
    "singular_name" => __( "ピックアップ", "sage" ),
  ];

  $args = [
    "label" => __( "ピックアップ", "sage" ),
    "labels" => $labels,
    "public" => false,
    "publicly_queryable" => true,
    "hierarchical" => false,
    "show_ui" => true,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "query_var" => true,
    "rewrite" => [ 'slug' => 'pickup', 'with_front' => true, ],
    "show_admin_column" => false,
    "show_in_rest" => true,
    "rest_base" => "pickup",
    "rest_controller_class" => "WP_REST_Terms_Controller",
    "show_in_quick_edit" => true,
    ];
  register_taxonomy( "pickup", [ "item" ], $args );
}
add_action( 'init', 'cptui_register_my_taxes_pickup' );

?>