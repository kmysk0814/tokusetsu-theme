<?php
// ------------------------------------------
// グローバルな設定記述１
//
// サムネイル投稿許可
// 画像取得時、サイズ情報付加を無効にする
// 画像に正方形サイズを追加
// ナビメニューのslugの追加
// ------------------------------------------

add_theme_support('post-thumbnails');

add_filter('wp_calculate_image_srcset_meta', '__return_null');

add_image_size( 'square', 240, 240, true );

register_nav_menus( array(
   'global' => 'グローバル',
   'side'   => 'サイド',
   'sub'   => 'サブ',
   'pickup'   => 'ピックアップ',
   'footer' => 'フッター'
) );


// ------------------------------------------
// グローバルな設定記述２
//
// 管理画面メニュー非表示
// ------------------------------------------


add_action( 'admin_menu', 'remove_menus' );
function remove_menus(){
    // remove_menu_page( 'index.php' ); //ダッシュボード
    // remove_menu_page( 'edit.php' ); //投稿メニュー
    // remove_menu_page( 'upload.php' ); //メディア
    // remove_menu_page( 'edit.php?post_type=page' ); //ページ追加
    remove_menu_page( 'edit-comments.php' ); //コメントメニュー
    // remove_menu_page( 'themes.php' ); //外観メニュー
    // remove_menu_page( 'plugins.php' ); //プラグインメニューa
    // remove_menu_page( 'tools.php' ); //ツールメニュー
    // remove_menu_page( 'options-general.php' ); //設定メニュー
       global $submenu;

}


// ------------------------------------------
// ショートコード経由でphpファイルを動かすためのコード
// [[有効化]]
// ショートコード [phpImport file='ファイル名']
// ------------------------------------------


//phpをショートコードで読み込む

function short_php($params = array()) {
  extract(shortcode_atts(array(
    'file' => 'default'
  ), $params));
  ob_start();
  include(get_theme_root() . '/' . get_template() . "/views/partials/$file.php");
  return ob_get_clean();
}

add_shortcode('phpImport', 'short_php');


function form_php($params = array()) {
    extract(shortcode_atts(array(
        'file' => 'default'
    ), $params));
    ob_start();
    include(get_theme_root() .'/'.get_template() . "/forms/$file.php");
    return ob_get_clean();
}

add_shortcode('formPhp', 'form_php');

?>
