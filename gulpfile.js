
const gulp = require('gulp');
// const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const plumber = require('gulp-plumber');

const paths = {
  'src': {
    'scss': './resources/assets/styles/**/*.scss',
  },
  'dist': {
    'css': './dist/styles/',
  }
};

gulp.task('sass', done => {
  gulp.src(paths.src.scss)
.pipe(plumber({
      errorHandler: function(err) {
        console.log(err.messageFormatted);
        this.emit('end');
      }
    }))
  .pipe(sourcemaps.init())
  .pipe(sass({
    outputStyle: 'expanded',
  }).on('error', sass.logError))
  // .pipe(autoprefixer({
    // browsers: ['last 2 versions'],
  // }))
  .pipe(sourcemaps.write())
  .pipe(gulp.dest(paths.dist.css))
  .pipe(cleanCSS())
  .pipe(rename({
    suffix: '.min',
  }))
  .pipe(gulp.dest(paths.dist.css));
  done();
});

gulp.task('default', () => {
  gulp.watch(paths.src.scss, gulp.task('sass'));
});
